module.exports = {
  transpileDependencies: ["vuetify"],
  publicPath: process.env.SUBDIR ? process.env.SUBDIR : "/",
};
