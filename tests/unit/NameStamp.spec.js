import { shallowMount } from "@vue/test-utils";
import NameStamp from "@/components/NameStamp.vue";

describe("NameStamp.vue", () => {
  it("contain svg element", () => {
    const wrapper = shallowMount(NameStamp, {
      propsData: {
        topName: "john",
        bottomName: "doe",
        date: "2000-01-01",
        showBox: false,
      },
    });
    expect(wrapper.find("svg").exists()).toBe(true);
  });

  it("show debug box", () => {
    const wrapper = shallowMount(NameStamp, {
      propsData: {
        topName: "john",
        bottomName: "doe",
        date: "2000-01-01",
        showBox: true,
      },
    });
    expect(wrapper.find("rect").exists()).toBe(true);
  });

  it("snapshot", () => {
    const wrapper = shallowMount(NameStamp, {
      propsData: {
        topName: "john",
        bottomName: "doe",
        date: "2000-01-01",
        showBox: true,
      },
    });
    expect(wrapper.element).toMatchSnapshot();
  });
});
